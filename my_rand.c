/* 
 * Code based on example from "FreeBSD Device Drivers A Guide for the Interpid" 
 * by Joseph Kong
 */

#include <sys/param.h>
#include <sys/module.h>
#include <sys/kernel.h>
#include <sys/systm.h>
#include <sys/conf.h>
#include <sys/uio.h>
#include <sys/malloc.h>
#include <sys/ioccom.h>
#include <sys/sysctl.h>
#include <sys/libkern.h>

/* Malloc type definition type for easy monitoring by 'vmstat -m' */
MALLOC_DEFINE(M_RAND, "random_buffer", "buffer for rand driver");

/* ioctl command definition. I/O operation without data transfer (29s) */
#define READ_RESET_RANGE _IO('R', 1)

/* Character device switch table callbacks */
static d_open_t rand_open;
static d_close_t rand_close;
static d_read_t rand_read;
static d_ioctl_t rand_ioctl;

/* Character device switch table callbacks (8s)*/
static struct cdevsw rand_cdevsw = { .d_version =    D_VERSION,
                                     .d_open =       rand_open,
                                     .d_close =      rand_close,
                                     .d_read =       rand_read,
                                     .d_ioctl =      rand_ioctl,
                                     .d_name =       "rand" };

/* Keep data in structs*/
typedef struct rand {
        int min;
        int max;
} rand_t;
static rand_t *rand_data;

static struct cdev *rand_dev;                   /* Global for make_dev */

/* Sysctl */
/* Sysctl context menagement (44s) */
static struct sysctl_ctx_list clist;            
/* Sysctl "root pointer" - used to refer root node (42s) */
static struct sysctl_oid *poid;                 

/* Switch table open procedure - open != loaded */
static int
rand_open(struct cdev *dev, int oflags, int devtype, struct thread *td)
{
        uprintf("Opening random generator device.\n");
        return (0);
}

/* Switch table close procedure */
static int
rand_close(struct cdev *dev, int fflag, int devtype, struct thread *td)
{
        uprintf("Closing random generator device.\n");
        return (0);
}

/* Switch table read */
static int
rand_read(struct cdev *dev, struct uio *uio, int ioflag)
{
        int error = 0;
        int to_copy, data_size = 0;
        int random_value;
	char rand_buffer[128] = "";

        if(uio->uio_offset == 0){
                if (rand_data->min >= rand_data->max){
                        uprintf("error - change min/max value \n");
                        return 0;
                }

                arc4random_buf(&random_value, sizeof(random_value));
                random_value = 
                        abs(random_value) % (rand_data->max - rand_data->min) + 
                        rand_data->min;
                data_size = sprintf(rand_buffer, "%d", random_value);
                rand_buffer[data_size]='\0';
        }
        to_copy = MIN(uio->uio_resid, (data_size - uio->uio_offset > 0) ? 
                      data_size - uio->uio_offset : 0);

        uprintf("Size %d %d\n", data_size, to_copy);
        /* Move data from kernel space -> user - same order as rand_write() */
        error = uiomove(rand_buffer, to_copy, uio);
        if (error != 0)
                uprintf("Read failed.\n");
        return (error);
}

/* ioctl function handler (36s) */
static int
rand_ioctl(struct cdev *dev, u_long cmd, caddr_t data, int fflag, 
           struct thread *td)
{
        int error = 0;
        switch (cmd) {
        case READ_RESET_RANGE:
                rand_data->max = 100;
                rand_data->min = -100;
                uprintf("Random - range reset.\n");
                break;
        default:
                /* “error: inappropriate ioctl for device” */
                error = ENOTTY;
                break;
        }
        return (error);
}


static int
rand_modevent(module_t mod __unused, int event, void *arg __unused)
{
        int error = 0;
        switch (event) {
        case MOD_LOAD:
                rand_data = malloc(sizeof(rand_t), M_RAND, M_WAITOK);
                rand_data->max = 100;
                rand_data->min = -100;
                /* Create sysctl structure */
                sysctl_ctx_init(&clist);
                poid = SYSCTL_ADD_ROOT_NODE(&clist, OID_AUTO,
                        "rand", CTLFLAG_RW, 0, "rand root node");
                SYSCTL_ADD_INT(&clist, SYSCTL_CHILDREN(poid), OID_AUTO,
                        "max", CTLFLAG_RW, &rand_data->max, 0, 
                        "random generator max value");
                SYSCTL_ADD_INT(&clist, SYSCTL_CHILDREN(poid), OID_AUTO,
                        "min", CTLFLAG_RW, &rand_data->min, 0, 
                        "random generator min value");
                /* Creates character device under /dev (9s) */
                rand_dev = make_dev(&rand_cdevsw, 0, UID_ROOT, GID_WHEEL,           
                        0600, "rand");
                uprintf("rand driver loaded.\n");
                break;
        case MOD_UNLOAD:
                /* Destroy character device node (9s) */
                destroy_dev(rand_dev);                                              
                sysctl_ctx_free(&clist);
                free(rand_data, M_RAND);
                uprintf("rand driver unloaded.\n");
                break;
        default:
                error = EOPNOTSUPP;
                break;
        }
        return (error);
}

/* 
 * Wrapper for DECLARE_MODULE macro (15s) which registers a KLD (loadable 
 * kernel module) and module event handler with the system (3s) 
 * rand - module name
 */
DEV_MODULE(rand, rand_modevent, NULL);